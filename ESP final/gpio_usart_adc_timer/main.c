/*****************************************************************************
  * @title   USART1,TIMER2,ADC1,GPIOs(input & output)
  * @author  Luqman Hakim, student of EEPIS(Electronics Engineering Polytechnic Institute of Surabaya)
  * @date    first build :12 Mar 2015
  *          latest build:25 Mar 2015
  * @brief   basic GPIO, UART,TIMER, and ADC example
  *          This project is made using Coocox CoIDE which is an open source ARM microcontroller developing IDE based on Eclipse.
  *          This project uses arm-none-eabi-gcc -mcpu as the toolchain. you have to use this toolchain if you develop under Coocox CoIDE.
  *******************************************************************************/
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_tim.h"
#include "stm32f10x_adc.h"
#include "misc.h"
#include <stdio.h>

#define LED_PORT GPIOB
#define LED0	GPIO_Pin_6
#define LED1	GPIO_Pin_7
#define LED2	GPIO_Pin_8
#define LED3	GPIO_Pin_9
#define button0 GPIO_Pin_4
#define button1 GPIO_Pin_5
#define button2 GPIO_Pin_14
#define button3 GPIO_Pin_15

uint16_t usart1_rx_data[],count=0;
uint16_t usart1_rx,data_x,data_y,receive=0,start=0;
int usart1_rx_num=0,i;
float f;

const  char error_msg[]="transmision error";
const  char finish_msg[]="transmision finished";
const  char timer3_int[]="timer3 interrupt";
//const unsigned char ADC_msg[]="";
char char_buff[];

void USART1_SendString(const unsigned char *stringBuff);
unsigned int read_adc1(unsigned char channel);

void USART1_IRQHandler(void) //USART1 ISR
{
    if ((USART1->SR & USART_FLAG_RXNE) != (u16)RESET)//USART1 RX ISR
    {
    	if(USART_ReceiveData(USART1)=='@')
    	{
    		start=1;
    		usart1_rx_num=1;
    		GPIO_ResetBits(LED_PORT,LED3);

    		sprintf(char_buff,"system core clock=%d  ",SystemCoreClock);
    		USART1_SendString(char_buff);
    	}
    	else if(usart1_rx_num==1)
    	{
    		data_x=USART_ReceiveData(USART1);usart1_rx_num++;
    		sprintf(char_buff,"datax=%c  ",data_x);
    		USART1_SendString(char_buff);
    	}
    	else if(usart1_rx_num==2)
    	{
    		data_y=USART_ReceiveData(USART1);usart1_rx_num++;
    		sprintf(char_buff,"datay=%c  ",data_y);
    		USART1_SendString(char_buff);
    	}
    	else if(usart1_rx_num==3 && USART_ReceiveData(USART1)!='$')
    	{
    		data_x=0; data_y=0; start=0; usart1_rx_num=0;
    		USART1_SendString(error_msg);

    	}
    	else if(usart1_rx_num==3 && USART_ReceiveData(USART1)=='$')
    	{
    		start=0; usart1_rx_num=0;
    		USART1_SendString(finish_msg);
    	}
	}

    /*if(data_x=='e')GPIO_ResetBits(LED_PORT,LED0);else
    if(data_y=='v')GPIO_SetBits(LED_PORT,LED1);

    if (start==1) GPIO_ResetBits(LED_PORT,LED3);else GPIO_SetBits(LED_PORT,LED3);
     */
}
void TIM2_IRQHandler(void)//Timer2 ISR
{
	if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)//timer2 overflow ISR
	{
		TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
		count++;
		if(count==60)count=0;
		//USART1_SendString(timer3_int,sizeof(timer3_int));
		sprintf(char_buff,"count=%2d ",count);
		USART1_SendString(char_buff);
		USART1_SendString("  hello, \n   ");
		GPIO_WriteBit(GPIOC,GPIO_Pin_13,GPIO_ReadOutputDataBit(GPIOC,GPIO_Pin_13)^1);
		//f=read_adc1(1)*0.0008056641;
		//sprintf(char_buff," adc=%4d",f);
		//USART1_SendString(char_buff);
	    //puts(char_buff);
	    //PutChar(char_buff);
	}
}
void USART1_SendString(const unsigned char *stringBuff) //send data string via USART1
{
    while(*stringBuff)// Loop while there are more characters to send.
    {
        USART_SendData(USART1, *stringBuff++);//
        while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET); //wait for next character to send
    }
}
void LED_Setup(void) //led on GPIOB 6,7,8,9
{

        GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_13;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	GPIO_Write(GPIOC,0x0000);//sets all pin to output low logic(0V)
}
void Button_Setup(void)//push buttons on GPIOA 14,15 and GPIOB 4,5
{
          GPIO_InitTypeDef GPIO_InitStruct;

	  //Configure GPIOA
	  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_GPIOA, ENABLE);
	  GPIO_InitStruct.GPIO_Pin = button3|button2;
	  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IPU;
	  GPIO_Init(GPIOA, &GPIO_InitStruct);

	  //*Configure GPIOB
	  GPIO_InitStruct.GPIO_Pin = button0|button1;
	  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IPU;
	  GPIO_Init(GPIOB, &GPIO_InitStruct);
}
void ClockSetup()//  system and peripheral clock setup
{
      RCC_DeInit ();                    /* RCC system reset(for debug purpose)*/
      RCC_HSEConfig (RCC_HSE_ON);       /* Enable HSE                         */

      /* Wait till HSE is ready                                               */
      while (RCC_GetFlagStatus(RCC_FLAG_HSERDY) == RESET);

      RCC_HCLKConfig   (RCC_SYSCLK_Div1);   /* HCLK   = SYSCLK                */
      RCC_PCLK2Config  (RCC_HCLK_Div1);     /* PCLK2  = HCLK                  */
      RCC_PCLK1Config  (RCC_HCLK_Div2);     /* PCLK1  = HCLK/2                */
      RCC_ADCCLKConfig (RCC_PCLK2_Div6);    /* ADCCLK = PCLK2/4               */

      /* PLLCLK = 8MHz * 9 = 72 MHz                                           */
      RCC_PLLConfig (0x00010000, RCC_PLLMul_9);

      RCC_PLLCmd (ENABLE);                  /* Enable PLL                     */

      /* Wait till PLL is ready                                               */
      while (RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET);

      /* Select PLL as system clock source                                    */
      RCC_SYSCLKConfig (RCC_SYSCLKSource_PLLCLK);

      /* Wait till PLL is used as system clock source                         */
      while (RCC_GetSYSCLKSource() != 0x08);

      /* Enable USART1 and GPIOA clock                                        */
      RCC_APB2PeriphClockCmd (RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOA, ENABLE);

}
void ADC1_calibration()
{
	//	ADC calibration (optional, but recommended at power on)
			ADC_ResetCalibration(ADC1);	// Reset previous calibration
			while(ADC_GetResetCalibrationStatus(ADC1));
			ADC_StartCalibration(ADC1);	// Start new calibration (ADC must be off at that time)
			while(ADC_GetCalibrationStatus(ADC1));

}
void ADC1_Setup()
{
        //enable peripheral clock for adc1 pinout(GPIOA)
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	//ADC1 channel 1(PA1)
		GPIO_InitTypeDef GPIO_InitStructure;
		GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AIN;
		GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_1 ;
		GPIO_Init(GPIOA, &GPIO_InitStructure);

	// ADC1 config
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1,ENABLE);

		ADC_InitTypeDef ADC_InitStructure;
		ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
		ADC_InitStructure.ADC_ScanConvMode = DISABLE;
		ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;	// we work in continuous sampling mode
		ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
		ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
		ADC_InitStructure.ADC_NbrOfChannel = 1;//initiate the number of used channel
		ADC_Init ( ADC1, &ADC_InitStructure);	//set config of ADC1
	// enable ADC1
		ADC_Cmd (ADC1,ENABLE);
	// calibrate ADC1
		ADC1_calibration();
}
void Timer2_Setup()
{
	/*	clock at 24 MHz the Prescaler is computed as following:
	     - Prescaler = (TIM3CLK / TIM3 counter clock) - 1
	    SystemCoreClock is set to 72 MHz for Low-density, Medium-density, High-density
	    and Connectivity line devices and to 24 MHz for Low-Density Value line and
	    Medium-Density Value line devices

	    The TIM3 is running at 36 KHz: TIM3 Frequency = TIM3 counter clock/(ARR + 1)
	                                                  = 24 MHz / 666 = 36 KHz

	    TIM3 Channel1 duty cycle = (TIM3_CCR1/ TIM3_ARR)* 100

	    ----------------------------------------------------------------------- */

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

	    /* Compute the prescaler value */
	uint32_t prescaled_val=2000000; //2MHz
	uint16_t TIM3_Tick=1;		   	//20KHz
	uint16_t PrescalerVal = (uint16_t) (SystemCoreClock / prescaled_val) - 1;

	TIM_TimeBaseInitTypeDef TIM_BaseInitStructure;
	TIM_BaseInitStructure.TIM_Prescaler=36000;
	TIM_BaseInitStructure.TIM_Period=(2000)-1;
	TIM_BaseInitStructure.TIM_ClockDivision=0;
	TIM_BaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM2, &TIM_BaseInitStructure);

	TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
	TIM_Cmd(TIM2, ENABLE);


}
void USART1_Setup()
{
	/***************************************************************************//**
	 * @brief Init USART1
	 ******************************************************************************/

      GPIO_InitTypeDef  GPIO_InitStructure;
      USART_InitTypeDef USART_InitStructure;

      /* Enable GPIOA clock                                                   */
      RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

      /* Configure USART1 Rx (PA10) as input floating                         */
      GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_10;
      GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN_FLOATING;
      GPIO_Init(GPIOA, &GPIO_InitStructure);

      /* Configure USART1 Tx (PA9) as alternate function push-pull            */
      GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_9;
      GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
      GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP;
      GPIO_Init(GPIOA, &GPIO_InitStructure);

      /* USART1 configured as follow:
            - BaudRate = 115200 baud
            - Word Length = 8 Bits
            - One Stop Bit
            - No parity
            - Hardware flow control disabled (RTS and CTS signals)
            - Receive and transmit enabled
            - USART Clock disabled
            - USART CPOL: Clock is active low
            - USART CPHA: Data is captured on the middle
            - USART LastBit: The clock pulse of the last data bit is not output to
                             the SCLK pin
      */
      USART_InitStructure.USART_BaudRate            = 115200;
      USART_InitStructure.USART_WordLength          = USART_WordLength_8b;
      USART_InitStructure.USART_StopBits            = USART_StopBits_1;
      USART_InitStructure.USART_Parity              = USART_Parity_No ;
      USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
      USART_InitStructure.USART_Mode                = USART_Mode_Rx | USART_Mode_Tx;
      USART_Init(USART1, &USART_InitStructure);

      USART_Cmd(USART1, ENABLE);
      USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
}
void TIM2_NVIC_Config()
{
	NVIC_InitTypeDef NVIC_InitStructure;

	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}
void USART1_NVIC_Config(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;

  /* Enable the USARTx Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}

unsigned int read_adc1(unsigned char channel)
{
	//USART1_SendString("conversion start",16);
	ADC_RegularChannelConfig(ADC1,channel, 1,ADC_SampleTime_28Cycles5);
	ADC_SoftwareStartConvCmd(ADC1,ENABLE);
	while (ADC_GetFlagStatus(ADC1,ADC_FLAG_EOC)==RESET);
		//USART1_SendString("converting",10);
	return ADC_GetConversionValue(ADC1);
}
int main(void)
{
	LED_Setup();
	Button_Setup();
	ClockSetup();
	USART1_Setup();
	Timer2_Setup();
	ADC1_Setup();
	USART1_NVIC_Config();
	TIM2_NVIC_Config();

	while(1)
    {
                /* running led
                GPIO_WriteBit(LED_PORT,GPIO_Pin_6,Bit_RESET);
                for (i=0;i<1000000;i++);
                GPIO_WriteBit(LED_PORT,GPIO_Pin_6,Bit_SET);

                GPIO_WriteBit(LED_PORT,GPIO_Pin_7,Bit_RESET);
                for (i=0;i<1000000;i++);
                GPIO_WriteBit(LED_PORT,GPIO_Pin_7,Bit_SET);

                GPIO_WriteBit(LED_PORT,GPIO_Pin_8,Bit_RESET);
                for (i=0;i<1000000;i++);
                GPIO_WriteBit(LED_PORT,GPIO_Pin_8,Bit_SET);

                GPIO_WriteBit(LED_PORT,GPIO_Pin_9,Bit_RESET);
                for (i=0;i<1000000;i++);
                GPIO_WriteBit(LED_PORT,GPIO_Pin_9,Bit_SET);
                */
		/* send data via usart1 if button is pressed
		while (GPIO_ReadInputDataBit(GPIOB,button0));
		USART_SendData(USART1,'e');

		while (!GPIO_ReadInputDataBit(GPIOB,button0));
		for (i=0;i<1000000;i++);

		if (!GPIO_ReadInputDataBit(GPIOB,button0))USART_SendData(USART1,'v');
                while (!GPIO_ReadInputDataBit(GPIOB,button0));
                for (i=0;i<1000000;i++);

                while (GPIO_ReadInputDataBit(GPIOB,button0));
                USART_SendData(USART1,'i');
                while (!GPIO_ReadInputDataBit(GPIOB,button0));
                for (i=0;i<1000000;i++);

                while (GPIO_ReadInputDataBit(GPIOB,button0));
                USART_SendData(USART1,' ');
                while (!GPIO_ReadInputDataBit(GPIOB,button0));
                for (i=0;i<1000000;i++);
		*/

		/*Send timer2 counter data via usart1
		sprintf(char_buff,"timer count=%d",TIM_GetCounter(TIM2));
		USART1_SendString(char_buff,30);
		*/
		/*toggle leds as button pressed
		GPIO_WriteBit(LED_PORT,GPIO_Pin_6,GPIO_ReadInputDataBit(GPIOA,button3));
		GPIO_WriteBit(LED_PORT,GPIO_Pin_7,GPIO_ReadInputDataBit(GPIOA,button2));
		GPIO_WriteBit(LED_PORT,GPIO_Pin_8,GPIO_ReadInputDataBit(GPIOB,button0));
		GPIO_WriteBit(LED_PORT,GPIO_Pin_9,GPIO_ReadInputDataBit(GPIOB,button1));
		*/
    }
}
