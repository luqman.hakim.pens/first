/*****************************************************************************
  * @title   USART1,TIMER2,ADC1,GPIOs(input & output)
  * @author  Luqman Hakim, student of EEPIS(Electronics Engineering Polytechnic Institute of Surabaya)
  * @date    first build :12 Mar 2015
  *          latest build:25 Mar 2015
  * @brief   basic GPIO, UART,TIMER, and ADC example
  *          This project is made using Coocox CoIDE which is an open source ARM microcontroller developing IDE based on Eclipse.
  *          This project uses arm-none-eabi-gcc -mcpu as the toolchain. you have to use this toolchain if you develop under Coocox CoIDE.
  *******************************************************************************/
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x.h"
#include "stm32f10x_usart.h"
#include "stm32f10x_tim.h"
#include "stm32f10x_iwdg.h"
#include "misc.h"
#include <stdio.h>

#define LED_PORT	GPIOC
#define LED0		GPIO_Pin_13
#define LED1		GPIO_Pin_7
#define LED2		GPIO_Pin_8
#define LED3		GPIO_Pin_9
#define button0 	GPIO_Pin_4
#define button1 	GPIO_Pin_5
#define button2 	GPIO_Pin_14
#define button3 	GPIO_Pin_15

#define out_channel_0 	GPIOA,GPIO_Pin_2
#define out_channel_1 	GPIOA,GPIO_Pin_3
#define out_channel_2 	GPIOA,GPIO_Pin_4
#define out_channel_3 	GPIOA,GPIO_Pin_5
#define out_channel_4 	GPIOA,GPIO_Pin_6
#define out_channel_5 	GPIOA,GPIO_Pin_7
#define out_channel_6 	GPIOB,GPIO_Pin_0
#define out_channel_7 	GPIOB,GPIO_Pin_1
#define led_out			GPIOC,GPIO_pin_13

#define led_togle		GPIOC->ODR ^= GPIO_Pin_13
#define led_on			GPIOC->BRR  = (1<<13)
#define led_off			GPIOC->BSRR = (1<<13)

#define buff_size	4

#define SSID 	"E"
#define PSWD 	"roboteducation12"
#define STA_IP 	"192.168.1.115"

uint16_t usart1_rx_data[],count=0;
uint16_t usart1_rx,data_x,data_y,receive=0,start=0,reply_trigger=0;
int usart1_rx_num=0,i;
float f;
char reconnect=0;

const  char error_msg[]="transmision error";
const  char finish_msg[]="transmision finished";
const  char timer3_int[]="timer3 interrupt";
//const unsigned char ADC_msg[]="";
 char char_buff[],char_buff2[],get_buff[4],get_count=0,get_char,rx_data;
unsigned char char_tmp,next_cmd=0;
unsigned char rec_buff[]="ready\r";
unsigned char prec_buff;

unsigned char data_sign=0, out_channel=0, out_state=1;
unsigned char rx_head=0,get_con_ch=0,ch_con=0;
unsigned char cnl[8]={1,1,1,1,1,1,1,1};
static volatile u32 delay_counter;
const  u32 Ticktime=1000000;
char sys_timeout=0;

typedef enum
{
	iwdg_timeout_1s = 1,
	iwdg_timeout_3s	= 2,
	iwdg_timeout_6s = 3,
	iwdg_timeout_13s= 4,
	iwdg_timeout_26s= 5
}iwdg_timeout;

void TimeTickDec();
void USART_SendString(USART_TypeDef* USARTx,char *stringBuff);
//void USART_SendString(const unsigned char *stringBuff);
FlagStatus USART_ReceiveStrMatch(USART_TypeDef* USARTx, char *rec_buff);
unsigned int read_adc1(unsigned char channel);
void set_out(unsigned char chn, unsigned char st);

void SysTick_Handler()// systick is used to generate precision system delay timing
{
  TimeTickDec();
}
void USART1_IRQHandler(void) //USART1 USB_SERIAL ISR
{
    if ((USART1->SR & USART_FLAG_RXNE) != (u16)RESET)//USART1 RX ISR
    {
    	USART_SendData(USART3,USART_ReceiveData(USART1));
    	led_on;

    	//if(USART_ReceiveStrMatch(USART1,"trisna\r\n"))USART_SendString(USART1,"hai");

    	if(get_con_ch==0 && USART_ReceiveData(USART1)=='I')get_con_ch=1;
		else if(get_con_ch==1 && USART_ReceiveData(USART1)=='P')get_con_ch=2;
		else if(get_con_ch==2 && USART_ReceiveData(USART1)=='D')get_con_ch=3;
		else if(get_con_ch==3 && USART_ReceiveData(USART1)==',')get_con_ch=4;
		else if(get_con_ch==4 && (USART_ReceiveData(USART1)>=0x30 && USART_ReceiveData(USART1)<=0x39))
		{
			ch_con=USART_ReceiveData(USART1) & 0x0F;
			get_con_ch=0;
			reply_trigger=2;
			sprintf(char_buff,"ch_con=%d\r\n",ch_con);
			USART_SendString(USART1,char_buff);
		}
		else
		{
			ch_con=0;
			get_con_ch=0;
			//USART_SendString(USART1,"nuLLIFIED");
		}
    }
}
void USART3_IRQHandler(void) //USART3 ESP ISR
{
    if ((USART3->SR & USART_FLAG_RXNE) != (u16)RESET)//USART1 RX ISR
    {
    	USART_SendData(USART1,USART_ReceiveData(USART3));led_on;

    	rx_data=USART_ReceiveData(USART3);

    	if(rx_head==0 && USART_ReceiveData(USART3)=='G')rx_head=1;
		else if(rx_head==1 && USART_ReceiveData(USART3)=='E')rx_head=2;
		else if(rx_head==2 && USART_ReceiveData(USART3)=='T')rx_head=3;
		else if(rx_head==3 && USART_ReceiveData(USART3)==' ')rx_head=4;
		else if(rx_head==4 && USART_ReceiveData(USART3)=='/')rx_head=5;

    	if(get_con_ch==0 && USART_ReceiveData(USART3)=='I')get_con_ch=1;
		else if(get_con_ch==1 && USART_ReceiveData(USART3)=='P')get_con_ch=2;
		else if(get_con_ch==2 && USART_ReceiveData(USART3)=='D')get_con_ch=3;
		else if(get_con_ch==3 && USART_ReceiveData(USART3)==',')get_con_ch=4;
		else if(get_con_ch==4 && (USART_ReceiveData(USART3)>=0x30 && USART_ReceiveData(USART3)<=0x39))
		{
			ch_con=USART_ReceiveData(USART3) & 0x0F;
			get_con_ch=0;
			reply_trigger=2;
			//sprintf(char_buff,"ch_con=%d\r\n",ch_con);
			//USART_SendString(USART1,char_buff);
		}
		else
		{
			//ch_con=0;
			get_con_ch=0;
			//USART_SendString(USART1,"nuLLIFIED");
		}


    	// ESP initiation status parsing

    	if(next_cmd==0)
		{
			if(USART_ReceiveStrMatch(USART3,"ready\r\n"))next_cmd=1;
		}
    	else if(next_cmd==1)
    	{
    		if(USART_ReceiveStrMatch(USART3,"OK\r\n"))next_cmd=2;
    	}
		else if(next_cmd==2)
		{
			if(USART_ReceiveStrMatch(USART3,"OK\r\n"))next_cmd=3;
			if(USART_ReceiveData(USART3)=='L')next_cmd=120;
		}
		else if(next_cmd==3)
		{
			if(USART_ReceiveStrMatch(USART3,"OK\r\n"))next_cmd=4;
		}
		else if(next_cmd==4)
		{
			if(USART_ReceiveStrMatch(USART3,"OK\r\n"))next_cmd=5;
		}
    	else if(next_cmd==5)
		{
			if(USART_ReceiveStrMatch(USART3,"OK\r\n"))
			{
				next_cmd=10;
				USART_SendString(USART1," ESP inititation sequence complete\r\n");
			}
		}
    	else if(next_cmd==10)
		{
			if(USART_ReceiveStrMatch(USART3,"WIFI DISCONNECT\r\n"))
			{
				next_cmd=100;
				//USART_SendString(USART1," ESP inititation sequence complete\r\n");
			}
		}
    	else if(next_cmd==100)
		{
			if(USART_ReceiveStrMatch(USART3,"WIFI GOT IP\r\n"))
			{
				next_cmd=3;
				USART_SendString(USART1,"hzz");
			}
		}

    	// command from ESP
    	if( (rx_head==5 || rx_head==6) && (USART_ReceiveData(USART3)== '@' || USART_ReceiveData(USART3)=='&'))
		{

			if(USART_ReceiveData(USART3)=='@')
			{
				rx_head=5;
				//USART1_SendString("INCOMING\n");
			}
			else if(USART_ReceiveData(USART3)=='&' && rx_head==5)
			{
				rx_head=6;
				data_sign=1;
				//USART1_SendString("START\n");
			}
		}
		if( rx_head==6 && (USART_ReceiveData(USART3)>=0x30 && USART_ReceiveData(USART3)<=0x39) )
		{
			//USART1_SendString("GET");
			if(data_sign==1)
			{
				out_channel=USART_ReceiveData(USART3) & 0x0F;
				data_sign++;
				//USART1_SendString("GET CHANNEL\n");
			}
			else if(data_sign==2)
			{
				out_state=USART_ReceiveData(USART3) & 0x0F;
				data_sign=0;
				rx_head=0;

				//USART1_SendString("GET STATE\n");
				//sprintf(char_buff,"\r\nOUT:(%d,%d)\r\n",out_channel,out_state);
				//USART_SendString(USART1,char_buff);

				set_out(out_channel,out_state);
				//reply_trigger=1;
			}
		}
    }
    USART_ClearFlag(USART3,USART_FLAG_RXNE);
}
void TIM2_IRQHandler(void)//Timer2 ISR
{
	if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)//timer2 overflow ISR
	{
		TIM_ClearITPendingBit(TIM2, TIM_IT_Update);

		count++;
		//USART_SendString(USART3,count);

		if(next_cmd==2)
		{
			sys_timeout++;
			//led_togle;

			if (sys_timeout==15)
			{
				sys_timeout=0;
				next_cmd=100;
				USART_SendString(USART1," connection FAIL\r\n");
			}
			//asm("b backdoor");
		}//goto goto_point;
		if(count==20)
		{
			next_cmd=100;
			count=0;
			USART_SendString(USART1," connection FAIL\r\n");
			//goto MAIN;
		}
	}

}

void USART_SendString(USART_TypeDef* USARTx,char *stringBuff) //send data string via USART1
{
    while(*stringBuff)// Loop while there are more characters to send.
    {
        USART_SendData(USARTx, *stringBuff++);//
        while(USART_GetFlagStatus(USARTx, USART_FLAG_TC) == RESET); //wait for next character to send
    }
}
FlagStatus USART_ReceiveStrMatch(USART_TypeDef* USARTx,char *rec_buff)
{
	FlagStatus Status=RESET;

	//get_count;
	if(*(rec_buff+get_count))
	{
		//USART_SendString(USARTx,"\nreceive:");
		//USART_SendData(USARTx,*(rec_buff+get_count));
		if(USART_ReceiveData(USARTx)== *(rec_buff+get_count))
		{
			//USART_SendString(USARTx," rec: ");
			//USART_SendData(USARTx,*(rec_buff+get_count));
			get_count++;
		}
		else
		{
			get_count=0;
			Status=RESET;
		}

		if(!*(rec_buff+get_count))
		{
			get_count=0;
			Status=SET;
			//USART_SendString(USARTx,"\nmatch cinta...\n");
		}
	}
	return Status;
}
void OUT_Setup(void) //led on GPIOB 6,7,8,9
{

    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
    GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_13;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOC, &GPIO_InitStructure); //LED ON PC13

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_0|GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOB, &GPIO_InitStructure); //LED ON PC13

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3|
									GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_Write(GPIOA,0xffff);//sets all pin to output high logic(3,3V)
	GPIO_Write(GPIOB,0xffff);//sets all pin to output high logic(3,3V)
	GPIO_Write(GPIOC,0xffff);//sets all pin to output high logic(3,3V)
}
void Button_Setup(void)//push buttons on GPIOA 14,15 and GPIOB 4,5
{
          GPIO_InitTypeDef GPIO_InitStruct;

	  //Configure GPIOA
	  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_GPIOB, ENABLE);
	  GPIO_InitStruct.GPIO_Pin = button3|button2;
	  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IPU;
	  GPIO_Init(GPIOA, &GPIO_InitStruct);

	  //*Configure GPIOB
	  GPIO_InitStruct.GPIO_Pin = button0|button1;
	  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IPU;
	  GPIO_Init(GPIOB, &GPIO_InitStruct);
}
void ClockSetup()//  system and peripheral clock setup
{
      RCC_DeInit ();                    /* RCC system reset(for debug purpose)*/
      RCC_HSEConfig (RCC_HSE_ON);       /* Enable HSE                         */

      /* Wait till HSE is ready                                               */
      while (RCC_GetFlagStatus(RCC_FLAG_HSERDY) == RESET);

      RCC_HCLKConfig   (RCC_SYSCLK_Div1);   /* HCLK   = SYSCLK                */
      RCC_PCLK2Config  (RCC_HCLK_Div1);     /* PCLK2  = HCLK                  */
      RCC_PCLK1Config  (RCC_HCLK_Div2);     /* PCLK1  = HCLK/2                */
      RCC_ADCCLKConfig (RCC_PCLK2_Div6);    /* ADCCLK = PCLK2/4               */

      /* PLLCLK = 8MHz * 9 = 72 MHz                                           */
      RCC_PLLConfig (0x00010000, RCC_PLLMul_9);

      RCC_PLLCmd (ENABLE);                  /* Enable PLL                     */

      /* Wait till PLL is ready                                               */
      while (RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET);

      /* Select PLL as system clock source                                    */
      RCC_SYSCLKConfig (RCC_SYSCLKSource_PLLCLK);

      /* Wait till PLL is used as system clock source                         */
      while (RCC_GetSYSCLKSource() != 0x08);

      /* Enable USART1 and GPIOA clock                                        */
      RCC_APB2PeriphClockCmd (RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOA, ENABLE);

}
void Timer2_Setup()
{
	/*	clock at 24 MHz the Prescaler is computed as following:
	     - Prescaler = (TIM3CLK / TIM3 counter clock) - 1
	    SystemCoreClock is set to 72 MHz for Low-density, Medium-density, High-density
	    and Connectivity line devices and to 24 MHz for Low-Density Value line and
	    Medium-Density Value line devices

	    The TIM3 is running at 36 KHz: TIM3 Frequency = TIM3 counter clock/(ARR + 1)
	                                                  = 24 MHz / 666 = 36 KHz

	    TIM3 Channel1 duty cycle = (TIM3_CCR1/ TIM3_ARR)* 100

	    ----------------------------------------------------------------------- */

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

	    /* Compute the prescaler value */
	uint32_t prescaled_val=2000000; //2MHz
	uint16_t TIM3_Tick=1;		   	//20KHz
	uint16_t PrescalerVal = (uint16_t) (SystemCoreClock / prescaled_val) - 1;

	TIM_TimeBaseInitTypeDef TIM_BaseInitStructure;
	TIM_BaseInitStructure.TIM_Prescaler=36000;
	TIM_BaseInitStructure.TIM_Period=(2000)-1;
	TIM_BaseInitStructure.TIM_ClockDivision=0;
	TIM_BaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM2, &TIM_BaseInitStructure);

	TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
	TIM_Cmd(TIM2, ENABLE);
}
void USART1_Setup()//USB SERIAL
{
	/***************************************************************************//**
	 * @brief Init USART1
	 ******************************************************************************/

      GPIO_InitTypeDef  GPIO_InitStructure;
      USART_InitTypeDef USART_InitStructure;

      /* Enable clock                                                   */
      RCC_APB2PeriphClockCmd (RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOA, ENABLE);

      /* Configure USART1 Rx (PA10) as input floating                         */
      GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_10;
      GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN_FLOATING;
      GPIO_Init(GPIOA, &GPIO_InitStructure);

      /* Configure USART1 Tx (PA9) as alternate function push-pull            */
      GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_9;
      GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
      GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP;
      GPIO_Init(GPIOA, &GPIO_InitStructure);

      /* USART1 configured as follow:
            - BaudRate = 115200 baud
            - Word Length = 8 Bits
            - One Stop Bit
            - No parity
            - Hardware flow control disabled (RTS and CTS signals)
            - Receive and transmit enabled
            - USART Clock disabled
            - USART CPOL: Clock is active low
            - USART CPHA: Data is captured on the middle
            - USART LastBit: The clock pulse of the last data bit is not output to
                             the SCLK pin
      */
      USART_InitStructure.USART_BaudRate            = 115200;
      USART_InitStructure.USART_WordLength          = USART_WordLength_8b;
      USART_InitStructure.USART_StopBits            = USART_StopBits_1;
      USART_InitStructure.USART_Parity              = USART_Parity_No ;
      USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
      USART_InitStructure.USART_Mode                = USART_Mode_Rx | USART_Mode_Tx;
      USART_Init(USART1, &USART_InitStructure);

      USART_Cmd(USART1, ENABLE);
      USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
}
void USART3_Setup()//ESP_8266
{
	/***************************************************************************//**
	 * @brief Init USART1
	 ******************************************************************************/

      GPIO_InitTypeDef  GPIO_InitStructure;
      USART_InitTypeDef USART_InitStructure;

      /* Enable GPIOA clock                                                   */
      RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
      RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3,ENABLE);

      /* Configure USART1 Rx (PB11) as input floating                         */
      GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_11;
      GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN_FLOATING;
      GPIO_Init(GPIOB, &GPIO_InitStructure);

      /* Configure USART1 Tx (PB10) as alternate function push-pull            */
      GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_10;
      GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
      GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP;
      GPIO_Init(GPIOB, &GPIO_InitStructure);

      /* USART1 configured as follow:
            - BaudRate = 115200 baud
            - Word Length = 8 Bits
            - One Stop Bit
            - No parity
            - Hardware flow control disabled (RTS and CTS signals)
            - Receive and transmit enabled
            - USART Clock disabled
            - USART CPOL: Clock is active low
            - USART CPHA: Data is captured on the middle
            - USART LastBit: The clock pulse of the last data bit is not output to
                             the SCLK pin
      */
      USART_InitStructure.USART_BaudRate            = 115200;
      USART_InitStructure.USART_WordLength          = USART_WordLength_8b;
      USART_InitStructure.USART_StopBits            = USART_StopBits_1;
      USART_InitStructure.USART_Parity              = USART_Parity_No ;
      USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
      USART_InitStructure.USART_Mode                = USART_Mode_Rx | USART_Mode_Tx;
      USART_Init(USART3, &USART_InitStructure);

      USART_Cmd(USART3, ENABLE);
      USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
}
uint8_t ESP_setup(char *ssid, char *pswd, char *sta_ip)
{
	//next_cmd=0;

	if(next_cmd==3)
	{
			reconnect=1;
			delay_ms(2500);
	}
	else delay_ms(500);

	TIM_Cmd(TIM2, DISABLE);

	if(next_cmd==0)
	{
		USART_SendString(USART3,"AT+RST\r\n");while(!next_cmd);delay_ms(1);
	}
	if(next_cmd==1)
	{
		USART_SendString(USART3,"AT+CWMODE=1\r\n");while(next_cmd==1);delay_ms(1);
	}

	// AT+CWLAP\r\n LISTING AVAILABLE AP
	//router login
	if(next_cmd==2)
	{	USART_SendString(USART3,"AT+CWJAP="); // AT+CWJAP="SSID","PSWD"\r\n

		USART_SendData(USART3,0x22);delay_ms(1);
		USART_SendString(USART3,ssid);delay_ms(1);  // SSID
		USART_SendData(USART3,0x22);delay_ms(1);
		USART_SendData(USART3,0x2C);delay_ms(1);

		USART_SendData(USART3,0x22);delay_ms(1);
		USART_SendString(USART3,pswd);delay_ms(1);  // PASSWORD
		USART_SendData(USART3,0x22);delay_ms(1);
		USART_SendString(USART3,"\r\n");

		TIM_Cmd(TIM2, ENABLE);
		while(next_cmd==2);delay_ms(1);
		TIM_Cmd(TIM2, DISABLE);
	}

	if(next_cmd==3 || reconnect==1)
	{
		next_cmd=3;

		USART_SendString(USART3,"AT+CIPSTA=");
		USART_SendData(USART3,0x22);delay_ms(1);
		USART_SendString(USART3,sta_ip);delay_ms(1);  //
		USART_SendData(USART3,0x22);delay_ms(1);
		USART_SendString(USART3,"\r\n");
		while(next_cmd==3);delay_ms(1);
	}

	if(next_cmd==4)
	{
		USART_SendString(USART3,"AT+CIPMUX=1\r\n");while(next_cmd==4);delay_ms(1);
	}
	if(next_cmd==5)
	{
		USART_SendString(USART3,"AT+CIPSERVER=1,80\r\n");while(next_cmd==5);
	}

	if(next_cmd==120)return 0;
	else return 1;

}
void ESP_println(char chn, char *str)
{
	char size=0;
	char ip=0;

	while(*str)
	{
		ip++;
		*str++;
		//USART_SendString(USART1,"^ ");
		//sprintf(char_buff,"%d , %d ,\r\n",str,ip);
		//USART_SendString(USART1,char_buff);
	}
	size=ip+1;
	str-=ip;

	sprintf(char_buff2,"AT+CIPSEND=%d,%d\r\n",chn,size);
	USART_SendString(USART3,char_buff2);
	while(USART_ReceiveData(USART3)!='>');//wait for '>'

	USART_SendString(USART3,str);
	USART_SendString(USART3,"\n");

	while(USART_ReceiveData(USART3)!='K'); //wait for SEND OK reply
	delay_ms(10);

	//while(rx_data!='s');delay_ms(5);
}
void TIM2_NVIC_Config()
{
	NVIC_InitTypeDef NVIC_InitStructure;

	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}
void USART1_NVIC_Config(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;

  /* Enable the USARTx Interrupt */
  NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
}
void USART3_NVIC_Config(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;

       /* Enable the USARTx Interrupt */
       NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
       NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
       NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
       NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
       NVIC_Init(&NVIC_InitStructure);
}
void SystickInit(u32 TickTime)
{
  while(SysTick_Config(SystemCoreClock/TickTime)!=0);
}
void TimeTickDec()
{
  if(delay_counter!=0)
    delay_counter--;
}
void delay_us(u32 us)
{
  delay_counter=us;
  while(delay_counter);
}
void delay_ms(u32 ms)
{
  while(ms--)
    delay_us(1000);
}
void IWDG_init(iwdg_timeout timeout)
{
	RCC->CSR |= RCC_CSR_RMVF; // clear reset flag

	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
	IWDG_SetReload(0x0fff);

	if		(timeout==iwdg_timeout_26s)IWDG_SetPrescaler(IWDG_Prescaler_256);
	else if	(timeout==iwdg_timeout_13s)IWDG_SetPrescaler(IWDG_Prescaler_128);
	else if	(timeout==iwdg_timeout_6s)IWDG_SetPrescaler(IWDG_Prescaler_64);
	else if	(timeout==iwdg_timeout_3s)IWDG_SetPrescaler(IWDG_Prescaler_32);
	else if	(timeout==iwdg_timeout_1s)IWDG_SetPrescaler(IWDG_Prescaler_16);
	 // 4, 8, 16 ... 256 //
	//This parameter must be a number between 0 and 0x0FFF. timeout = 13s = reload/(LSI clk / prescaller) = 4095/(~40khz/128)
	IWDG_ReloadCounter();
	IWDG_Enable();
}
uint8_t iwdg_check_rst()
{
	uint8_t result = 0;

	if (RCC->CSR & RCC_CSR_IWDGRSTF)
	{
		/* Reset by IWDG */
		result = 1;

		/* Clear reset flags */
		RCC->CSR |= RCC_CSR_RMVF;
	}

	return result;
}
void set_out(unsigned char chn, unsigned char st)
{
	switch (chn)
	{
		case 0:	GPIO_WriteBit(out_channel_0,st); cnl[0]=st; break;
		case 1:	GPIO_WriteBit(out_channel_1,st); cnl[1]=st; break;
		case 2:	GPIO_WriteBit(out_channel_2,st); cnl[2]=st; break;
		case 3:	GPIO_WriteBit(out_channel_3,st); cnl[3]=st; break;
		case 4:	GPIO_WriteBit(out_channel_4,st); cnl[4]=st; break;
		case 5:	GPIO_WriteBit(out_channel_5,st); cnl[5]=st; break;
		case 6:	GPIO_WriteBit(out_channel_6,st); cnl[6]=st; break;
		case 7:	GPIO_WriteBit(out_channel_7,st); cnl[7]=st; break;
		case 8:	{
					if(st)
					{
						GPIO_Write(GPIOA,0xffff);
						GPIO_Write(GPIOB,0xffff);
					}
					else
					{
						GPIO_Write(GPIOA,0xff03);
						GPIO_Write(GPIOB,0xffc0);
					}

					break;
				}

		default:
			break;
	}
}


int main(void)
{
	ClockSetup();
	SystickInit(1000000);// systick update every 1us, 1MHz frequency
	USART1_Setup();USART1_NVIC_Config();
	USART3_Setup();USART3_NVIC_Config();
	Timer2_Setup();TIM2_NVIC_Config();TIM_Cmd(TIM2, DISABLE);
	OUT_Setup();

	if(iwdg_check_rst()) USART_SendString(USART1,"IWDG\n");
	else USART_SendString(USART1,"\nNON IWDG\n");

	//Enable Watchdog
	IWDG_init(iwdg_timeout_26s);

	//goto_point:
#pragma asm
	back:
#pragma endasm

	asm("backdoor:");

	next_cmd=0;
	reply_trigger=0;
	led_on;
	ESP_setup(SSID,PSWD,STA_IP);

	Button_Setup();
	set_out(1,1);
	led_off;

	USART_SendString(USART1,"Ready to go\n");


	while(1)
    {

		IWDG_ReloadCounter();
		sys_timeout=0;

		led_togle;
		//ESP_println("test");
		if(next_cmd==100)delay_ms(100);
		else if(next_cmd==3)ESP_setup(SSID,PSWD,STA_IP);
		else delay_ms(1000);

		if(reply_trigger==1)
		{
			reply_trigger=0;
			out_channel=0;
			out_state=0;
		}
		else if(reply_trigger==2)
		{
			delay_ms(50);
			ESP_println(ch_con,"HTTP/1.1 200 OK");
			ESP_println(ch_con,"Content-Type: text/html");
			ESP_println(ch_con,"");  //  do not forget this one
			ESP_println(ch_con,"<!DOCTYPE HTML>");
			ESP_println(ch_con,"<html>");
			ESP_println(ch_con,"<fieldset>");
			ESP_println(ch_con,"<font color = red>");
			ESP_println(ch_con,"ESP 8 Relay Control");
			ESP_println(ch_con,"</font>");
			ESP_println(ch_con,"</fieldset>");
			ESP_println(ch_con,"<br><br>");

			ESP_println(ch_con,"RELAY1 is: ");
			if(cnl[0])
			{
				ESP_println(ch_con,"OFF");
				ESP_println(ch_con,"<a href=\"/@&00\"\"><button>TURN ON </button></a>");
			}
			else
			{
				ESP_println(ch_con,"ON");
				ESP_println(ch_con,"<a href=\"/@&01\"\"><button>TURN OFF </button></a>");
			}
			ESP_println(ch_con,"<br><br>");

			ESP_println(ch_con,"RELAY2 is: ");
			if(cnl[1])
			{
				ESP_println(ch_con,"OFF");
				ESP_println(ch_con,"<a href=\"/@&10\"\"><button>TURN ON </button></a>");
			}
			else
			{
				ESP_println(ch_con,"ON");
				ESP_println(ch_con,"<a href=\"/@&11\"\"><button>TURN OFF </button></a>");
			}
			ESP_println(ch_con,"<br><br>");

			ESP_println(ch_con,"RELAY3 is: ");
			if(cnl[2])
			{
				ESP_println(ch_con,"OFF");
				ESP_println(ch_con,"<a href=\"/@&20\"\"><button>TURN ON </button></a>");
			}
			else
			{
				ESP_println(ch_con,"ON");
				ESP_println(ch_con,"<a href=\"/@&21\"\"><button>TURN OFF </button></a>");
			}
			ESP_println(ch_con,"<br><br>");

			ESP_println(ch_con,"RELAY4 is: ");
			if(cnl[3])
			{
				ESP_println(ch_con,"OFF");
				ESP_println(ch_con,"<a href=\"/@&30\"\"><button>TURN ON </button></a>");
			}
			else
			{
				ESP_println(ch_con,"ON");
				ESP_println(ch_con,"<a href=\"/@&31\"\"><button>TURN OFF </button></a>");
			}
			ESP_println(ch_con,"<br><br>");

			ESP_println(ch_con,"RELAY5 is: ");
			if(cnl[4])
			{
				ESP_println(ch_con,"OFF");
				ESP_println(ch_con,"<a href=\"/@&40\"\"><button>TURN ON </button></a>");
			}
			else
			{
				ESP_println(ch_con,"ON");
				ESP_println(ch_con,"<a href=\"/@&41\"\"><button>TURN OFF </button></a>");
			}
			ESP_println(ch_con,"<br><br>");

			ESP_println(ch_con,"RELAY6 is: ");
			if(cnl[5])
			{
				ESP_println(ch_con,"OFF");
				ESP_println(ch_con,"<a href=\"/@&50\"\"><button>TURN ON </button></a>");
			}
			else
			{
				ESP_println(ch_con,"ON");
				ESP_println(ch_con,"<a href=\"/@&51\"\"><button>TURN OFF </button></a>");
			}
			ESP_println(ch_con,"<br><br>");

			ESP_println(ch_con,"RELAY7 is: ");
			if(cnl[6])
			{
				ESP_println(ch_con,"OFF");
				ESP_println(ch_con,"<a href=\"/@&60\"\"><button>TURN ON </button></a>");
			}
			else
			{
				ESP_println(ch_con,"ON");
				ESP_println(ch_con,"<a href=\"/@&61\"\"><button>TURN OFF </button></a>");
			}
			ESP_println(ch_con,"<br><br>");

			ESP_println(ch_con,"RELAY8 is: ");
			if(cnl[7])
			{
				ESP_println(ch_con,"OFF");
				ESP_println(ch_con,"<a href=\"/@&70\"\"><button>TURN ON </button></a>");
			}
			else
			{
				ESP_println(ch_con,"ON");
				ESP_println(ch_con,"<a href=\"/@&71\"\"><button>TURN OFF </button></a>");
			}
			ESP_println(ch_con,"<br><br>");

			ESP_println(ch_con,"</html>");
			ESP_println(ch_con,"<br><br>");

			sprintf(char_buff,"AT+CIPCLOSE=%d\r\n",ch_con);delay_ms(500);
			USART_SendString(USART3,char_buff);//delay_ms(1000);

			/*delay_ms(50);
			char_tmp=sizeof("\r\nch_con:d)\r\n")-1;
			sprintf(char_buff2,"AT+CIPSEND=%d,%d\r\n",ch_con,char_tmp);
			USART_SendString(USART3,char_buff2);delay_ms(10);

			sprintf(char_buff,"\r\nch_con:%d)\r\n",ch_con);
			USART_SendString(USART3,char_buff);delay_ms(100);
			sprintf(char_buff2,"AT+CIPCLOSE=%d,\r\n",ch_con);
*/
			/*sprintf(char_buff,"HTTP/1.1 200 OK\n");
			char_tmp=sizeof(char_buff)-1;
			sprintf(char_buff2,"AT+CIPSEND=%d,%d\r\n",ch_con,char_tmp);
			USART_SendString(USART3,char_buff2);delay_ms(50);
			USART_SendString(char_buff);

			sprintf(char_buff,"Content-Type text/html\n");
			char_tmp=sizeof(char_buff)-1;
			sprintf(char_buff2,"AT+CIPSEND=%d,%d\r\n",ch_con,char_tmp);
			USART_SendString(USART3,char_buff2);delay_ms(50);
			USART_SendString(char_buff);

			sprintf(char_buff,"");
			char_tmp=sizeof(char_buff)-1;
			sprintf(char_buff2,"AT+CIPSEND=%d,%d\r\n",ch_con,char_tmp);
			USART_SendString(USART3,char_buff2);delay_ms(50);
			USART_SendString(char_buff);
*/
			reply_trigger=0;
		}
    }
}
